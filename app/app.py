from os import environ, path

import click
from flask import Flask, send_from_directory
from flask_smorest import Api

from .cache import cache
from .db import db, init_db
from .users import bp as users_bp


def create_app():
    app = Flask(__name__)
    app.config["API_TITLE"] = "User Lister"
    app.config["API_VERSION"] = "v1"

    app.config["OPENAPI_VERSION"] = "3.0.2"
    app.config["OPENAPI_URL_PREFIX"] = "/"
    app.config["OPENAPI_SWAGGER_UI_PATH"] = "/ui"
    app.config["OPENAPI_SWAGGER_UI_URL"] = "https://cdn.jsdelivr.net/npm/swagger-ui-dist/"
    app.config["OPENAPI_REDOC_PATH"] = "/doc"
    app.config["OPENAPI_REDOC_URL"] = "https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js"

    app.config.from_prefixed_env()

    if not app.config.get("SQLALCHEMY_DATABASE_URI") and environ.get("DATABASE_URL"):
        app.config["SQLALCHEMY_DATABASE_URI"] = environ[
            "DATABASE_URL"  # alternative variable set by heroku
        ].replace(
            "postgres://", "postgresql://", 1  # postgres deprecated by SQLAlchemy
        )
    # SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be disabled by default in the future
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    if not app.config.get("CACHE_REDIS_URL") and environ.get("REDIS_URL"):
        app.config["CACHE_REDIS_URL"] = environ["REDIS_URL"]
        app.config["CACHE_TYPE"] = "RedisCache"
    elif not app.config.get("CACHE_CACHE_TYPE"):
        app.config["CACHE_CACHE_TYPE"] = "SimpleCache"
    app.config["CACHE_KEY_PREFIX"] = app.config.get("CACHE_KEY_PREFIX") or "app:"
    cache.init_app(app)  # flask-caching defaults timeout to 300s

    @app.route("/")
    def index():
        return {"hi": "there"}

    @app.route("/favicon.ico")
    def favicon():
        return send_from_directory(
            path.join(app.root_path, "static"),
            "favicon.ico",
            mimetype="image/vnd.microsoft.icon",
        )

    @app.cli.command("init-db")
    def init_db_command():
        """Clear existing data and create new tables."""
        init_db()
        click.echo("Initialized the database.")

    api = Api(app)
    api.register_blueprint(users_bp, url_prefix="/users")

    return app
