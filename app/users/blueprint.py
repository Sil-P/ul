from flask import make_response, Response
from flask.views import MethodView
from flask_smorest import Blueprint
from flask_smorest.error_handler import ErrorSchema
from flask_smorest.pagination import PaginationParameters
import marshmallow as ma
import marshmallow_sqlalchemy as mas

from .models import User
from ..cache import cache


bp = Blueprint("users", __name__)


def request_revalidation(response: Response):
    response.cache_control.no_cache = True
    return response


bp.after_app_request(request_revalidation)


class UserFilter(ma.Schema):
    username = ma.fields.String()
    first_name = ma.fields.String()
    last_name = ma.fields.String()
    email = ma.fields.Email()
    city = ma.fields.String()
    state = ma.fields.String(validate=lambda v: len(v) == 2)
    zip = ma.fields.String()
    region = ma.fields.String()


class UserSchema(mas.SQLAlchemyAutoSchema):
    class Meta:
        model = User

    id = mas.auto_field(load_only=True)  # exclude from response
    current_company = mas.fields.Related(["name"])
    current_job = mas.fields.Related(["name"])


@bp.route("/")
class Users(MethodView):
    @cache.cached(query_string=True)
    @bp.arguments(UserFilter, location="query")
    @bp.etag
    @bp.response(200, UserSchema(many=True))
    @bp.alt_response(404, schema=ErrorSchema)
    @bp.paginate()
    def get(self, filters, pagination_parameters: PaginationParameters):
        """List users that fullfill filters"""
        users_page = (
            User.query.filter_by(**filters)
            .order_by(User.last_update.desc())
            .paginate(
                page=pagination_parameters.page,
                per_page=pagination_parameters.page_size,
                error_out=False,
                max_per_page=pagination_parameters.page_size,
            )
        )
        pagination_parameters.item_count = users_page.total
        response_items = list(users_page.items)
        bp.set_etag({user.id: user.last_update.isoformat() for user in response_items})
        if not users_page.total:
            return make_response(  # abort does not set etag, pagination, etc.
                ErrorSchema().load(
                    {
                        "message": "No users found for the filters",
                        "code": 404,
                        "status": "Not Found",
                    }
                ),
                404,
            )
        return response_items
