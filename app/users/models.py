from datetime import datetime

from ..db import db


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False, unique=True)


class Job(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(), nullable=False, unique=True)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(), unique=True, nullable=False)
    joining_date = db.Column(db.Date, nullable=False)
    last_update = db.Column(
        db.TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False
    )
    last_login = db.Column(
        db.TIMESTAMP, default=datetime.utcnow, onupdate=datetime.utcnow
    )
    first_name = db.Column(db.String(), nullable=False)
    last_name = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
    phone = db.Column(db.String())
    city = db.Column(db.String())  # TODO: create relation
    state = db.Column(db.String(2))  # TODO: use enum
    zip = db.Column(db.String())
    region = db.Column(db.String())  # TODO: use enum
    current_company_id = db.Column(db.Integer, db.ForeignKey(Company.id))
    current_company = db.relationship(Company, lazy="joined")
    current_job_id = db.Column(db.Integer, db.ForeignKey(Job.id))
    current_job = db.relationship(Job, lazy="joined")
