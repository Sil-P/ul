# Populate test DB

## Initialize schema

Run `flask init-db` from the app dir (for example from the test container shell).

Environment variables:
* `DATABASE_URL`: postgresql DB connection URL.

## Seed DB

Import csv files to postgresql DB (COPY ... WITH CSV)

After trying to generate the DB using [Faker](https://faker.readthedocs.io/) and [Synth](https://www.getsynth.com/docs/), and stumbling into duplicated values, the final approach is a mix of several sources.

### `company.csv`

Generated from `company.json` by running

    (. ../.env && synth generate . --to postgresql://${DBUSER}:${DBPASSWORD}@localhost:5432/${DBNAME} )

(38673 is the number of unique companies that Synth can generate)

### `job.csv`

From [Faker job provider](https://github.com/joke2k/faker/blob/v14.1.0/faker/providers/job/__init__.py), because Synth has much fewer "professions".


### `Hr1m.csv`

From https://eforexcel.com/wp/downloads-16-sample-csv-files-data-sets-for-testing/

Schema (loosely infered by [csvsql](https://csvkit.readthedocs.io/en/1.0.7/scripts/csvsql.html), that failed to import the csv performantly :/):

    CREATE TABLE hr (
    "Emp ID" DECIMAL NOT NULL,
    "Name Prefix" VARCHAR NOT NULL,
    "First Name" VARCHAR NOT NULL,
    "Middle Initial" VARCHAR NOT NULL,
    "Last Name" VARCHAR NOT NULL,
    "Gender" VARCHAR NOT NULL,
    "E Mail" VARCHAR NOT NULL,
    "Father's Name" VARCHAR NOT NULL,
    "Mother's Name" VARCHAR NOT NULL,
    "Mother's Maiden Name" VARCHAR NOT NULL,
    "Date of Birth" DATE NOT NULL,
    "Time of Birth" VARCHAR NOT NULL,
    "Age in Yrs." DECIMAL NOT NULL,
    "Weight in Kgs." DECIMAL NOT NULL,
    "Date of Joining" DATE NOT NULL,
    "Quarter of Joining" VARCHAR NOT NULL,
    "Half of Joining" VARCHAR NOT NULL,
    "Year of Joining" DECIMAL NOT NULL,
    "Month of Joining" DECIMAL NOT NULL,
    "Month Name of Joining" VARCHAR NOT NULL,
    "Short Month" VARCHAR NOT NULL,
    "Day of Joining" DECIMAL NOT NULL,
    "DOW of Joining" VARCHAR NOT NULL,
    "Short DOW" VARCHAR NOT NULL,
    "Age in Company (Years)" DECIMAL NOT NULL,
    "Salary" DECIMAL NOT NULL,
    "Last %% Hike" VARCHAR NOT NULL,
    "SSN" VARCHAR NOT NULL,
    "Phone No. " VARCHAR NOT NULL,
    "Place Name" VARCHAR NOT NULL,
    "County" VARCHAR NOT NULL,
    "City" VARCHAR NOT NULL,
    "State" VARCHAR NOT NULL,
    "Zip" DECIMAL NOT NULL,
    "Region" VARCHAR NOT NULL,
    "User Name" VARCHAR NOT NULL,
    "Password" VARCHAR NOT NULL
    );

## Populate `user` table

    insert into "user" (
      username, joining_date
    , first_name, last_name, email, phone
    , city, state, zip, region
    , last_update, last_login
    , current_company_id, current_job_id
    )
    select
      concat("User Name", row_number() OVER (PARTITION BY "User Name" ORDER BY "Emp ID"))
    , "Date of Joining"
    , "First Name", "Last Name", "E Mail", "Phone No. "
    , "City", "State", "Zip", "Region"
    , case when random() > .2 then "Date of Joining" + random() * interval '22 hour'
      else "Date of Joining" + (random() * (now() - "Date of Joining")) end
    , case when random() < .1 then null
      else "Date of Joining" + (random() * (now() - "Date of Joining")) end
    , case when random() < .05 then null
      else floor(random() * (select count(*) from company)) + 1 end
    , case when random() < .05 then null
      else floor(random() * (select count(*) from job)) + 1 end
    from hr
