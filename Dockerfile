FROM python:3.10-alpine AS base
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

FROM base as dev
CMD FLASK_RUN_PORT=${PORT:-5000} FLASK_RUN_HOST=0.0.0.0 flask run

FROM dev as test
COPY app .
