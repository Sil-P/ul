## Current features

**API endpoint** to list users data  
Documented as OpenAPI at `/openapi.json`,
accessible through ReDoc at `/doc` and Swagger at `/ui`  
List is sorted by user last update (from most to least recent).

**Test database with 1MM users records**  
Postgresql database, check [test_data](./test_data/) to learn about its generation.  
In English, with US addresses.

**Filtration support**  
By specific user attributes values, like name, city, etc.

**Pagination support**  
Translated to DB-level pagination.

**Cache Control**  
Client-side using `Cache-Control` and `Etag` headers.  
  
Server-side configurable to use redis or instance memory.  
Uses request path and query params to determine staleness,
it's limited to 5 minutes because it does not expire when the response changes.


### See online

Changes in `main` branch are automatically published at https://stag-pbw.herokuapp.com/ (first request may take a while because heroku pauses it).


## Pending improvements

**API**  
CRUD capabilities for `user`, `job`, `company`

**Filtration and sorting**  
Filtering by related attributes (job, company).  
Partial match on fields like name, email.  
Structure state, region to make them selectable from a predetermined list options.  
Options to combine filters (and/or).  
Sorting support.  

**Pagination support**  
Document configuration of default pagination values.  
Sync DB max page size with pagination config.  

**Cache Control**  
Invalidation strategy.

**Infrastructure**  
Heroku is complaining about too many rows in the DB, point to the RDS DB instead.  
Review redis cache: heroku app seems to be having issues writing to the redislab instance (works on my machine :/)


## Configuring and running locally

Deploy locally with `docker compose up`, application will be available at `http://localhost:<PORT>`

Environment variables:  
`PORT`: application port, defaults to `5000`.

Application can be configured by setting `FLASK_` environment variables, for example:  
`FLASK_SQLALCHEMY_DATABASE_URI`: DB connection URL (postgresql expected, other backends may require additional dependencies and adapting code).  
`FLASK_CACHE_DEFAULT_TIMEOUT`: cache entries TTL, in seconds (default=300).  
`FLASK_CACHE_KEY_PREFIX`: prefix for cache entries when using redis.

Alternative environment variables:  
`DATABASE_URL`: postgresql DB connection URL.  
`REDIS_URL`: redis connection URL to be used as cache, uses simple memory cache when not provided. Disable cache completely by setting `FLASK_CACHE_TYPE=NullCache` instead.
